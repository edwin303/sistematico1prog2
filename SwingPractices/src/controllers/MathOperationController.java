/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import views.CalculatorForm;

/**
 *
 * @author William Sánchez
 */
public class MathOperationController implements ActionListener {
    private final CalculatorForm calc;
    public MathOperationController(CalculatorForm f){
        calc = f;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "0": case "1": case "2": case "3":
            case "4": case "5": case "6": case "7":
            printNumber(e);
                break;
            case "+":case "-":case "*":case "/":
            case "n!":case ".nf.":case "n/x?":case "0112...":
                break;
        }
    }
    private void printNumber(ActionEvent evt) {                                            
        JButton b = (JButton)evt.getSource();
        if (calc.isFreeToStart()){
            calc.clear();
            calc.setTextToScreen(b.getText());            
        }
        else
        {
            if(!calc.getScreenText().equals("0")){
                calc.setTextToScreen(calc.getScreenText()+ b.getText());
            }
            else
            {
                calc.setTextToScreen(b.getText());
            }            
        }
    }
    
}
