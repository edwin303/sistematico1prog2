/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author William Sanchez
 */

public class Book implements Serializable{

    private int title;
    private String category;
    private Double author;
    private String precio;
    private String publisher;
    private String description;

    public Book() {
        this.title = 0;
        this.category = "";
        this.author = 0.0;
        this.publisher = "";
        this.description = "";
        this.precio = "";
    }

    public Book(int title, String category, Double author, String Precio, String publisher, String description) {
        this.title = title;
        this.category = category;
        this.author = author;
        this.publisher = publisher;
        this.description = description;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = Integer.parseInt(title);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = Double.parseDouble(author);
        
        
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}